package ru.smochalkin.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.api.endpoint.IProjectEndpoint;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.dto.ProjectDto;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.dto.result.Fail;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.result.Success;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    @SneakyThrows
    public Result createProject(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        sessionService.validate(sessionDto);
        try {
            projectService.create(sessionDto.getUserId(), name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeProjectStatusById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "status") @NotNull final String status
    ) {
        sessionService.validate(sessionDto);
        try {
            projectService.updateStatusById(sessionDto.getUserId(), id, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeProjectStatusByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "status") @NotNull final String status
    ) {
        sessionService.validate(sessionDto);
        try {
            projectService.updateStatusByIndex(sessionDto.getUserId(), index, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeProjectStatusByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "status") @NotNull final String status
    ) {
        sessionService.validate(sessionDto);
        try {
            projectService.updateStatusByName(sessionDto.getUserId(), name, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<ProjectDto> findProjectAllSorted(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "sort") @NotNull final String strSort
    ) {
        sessionService.validate(sessionDto);
        return projectService.findAll(sessionDto.getUserId(), strSort);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<ProjectDto> findProjectAll(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto
    ) {
        sessionService.validate(sessionDto);
        return projectService.findAll(sessionDto.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectDto findProjectById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id
    ) {
        sessionService.validate(sessionDto);
        return projectService.findById(sessionDto.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectDto findProjectByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name
    ) {
        sessionService.validate(sessionDto);
        return projectService.findByName(sessionDto.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public ProjectDto findProjectByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        sessionService.validate(sessionDto);
        return projectService.findByIndex(sessionDto.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeProjectById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id
    ) {
        sessionService.validate(sessionDto);
        try {
            projectService.removeById(sessionDto.getUserId(), id);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeProjectByName(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "name") @NotNull final String name
    ) {
        sessionService.validate(sessionDto);
        try {
            projectService.removeByName(sessionDto.getUserId(), name);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeProjectByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        sessionService.validate(sessionDto);
        try {
            projectService.removeByIndex(sessionDto.getUserId(), index);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result clearProjects(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto
    ) {
        sessionService.validate(sessionDto);
        try {
            projectService.clear(sessionDto.getUserId());
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateProjectById(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        sessionService.validate(sessionDto);
        try {
            projectService.updateById(sessionDto.getUserId(), id, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateProjectByIndex(
            @WebParam(name = "session") @NotNull final SessionDto sessionDto,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        sessionService.validate(sessionDto);
        try {
            projectService.updateByIndex(sessionDto.getUserId(), index, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
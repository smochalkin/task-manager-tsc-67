package ru.smochalkin.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.api.service.IUserService;
import ru.smochalkin.tm.dto.UserDto;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyEmailException;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyLoginException;
import ru.smochalkin.tm.exception.empty.EmptyPasswordException;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.exception.system.LoginExistsException;
import ru.smochalkin.tm.repository.dto.UserRepository;
import ru.smochalkin.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

@Service
public class UserDtoService extends AbstractDtoService<UserDto> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isLogin(login)) throw new LoginExistsException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        @NotNull final UserDto user = new UserDto();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(getPasswordHash(password));
        user.setEmail(email);
        userRepository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        userRepository.deleteAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDto> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDto findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return userRepository.findById(id).orElse(null);
    }

    @Nullable
    public UserDto findByLogin(@NotNull final String login) {
        return userRepository.findFirstByLogin(login);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByLogin(@NotNull final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        userRepository.deleteByLogin(login);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @Nullable final UserDto user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        @Nullable final String hash = getPasswordHash(password);
        user.setPasswordHash(hash);
        userRepository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @Nullable final UserDto user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userRepository.save(user);
    }

    @Override
    @SneakyThrows
    public boolean isLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        return Optional.ofNullable(userRepository.findFirstByLogin(login)).isPresent();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final UserDto user = findByLogin(login);
        if (user == null) throw new EntityNotFoundException();
        user.setLock(true);
        userRepository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final UserDto user = findByLogin(login);
        if (user == null) throw new EntityNotFoundException();
        user.setLock(false);
        userRepository.save(user);
    }

    @Override
    @SneakyThrows
    public int getCount() {
        return (int) userRepository.count();
    }

    @NotNull
    private String getPasswordHash(@NotNull final String password) {
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        return HashUtil.salt(password, secret, iteration);
    }

}


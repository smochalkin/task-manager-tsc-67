package ru.smochalkin.tm.model;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.listener.JpaEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@EntityListeners(JpaEntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project extends AbstractBusinessEntity {

    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

}
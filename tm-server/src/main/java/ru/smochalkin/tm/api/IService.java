package ru.smochalkin.tm.api;

import ru.smochalkin.tm.dto.AbstractEntityDto;

public interface IService <E extends AbstractEntityDto> extends IRepository<E> {
}

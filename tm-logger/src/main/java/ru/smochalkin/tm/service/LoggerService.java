package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.smochalkin.tm.api.service.ILoggerService;
import ru.smochalkin.tm.dto.LogDto;

import java.io.File;
import java.io.FileOutputStream;

@Service
public class LoggerService implements ILoggerService {

    @NotNull
    final public static String PROJECT_LOG_FILE_NAME = "./logger/project.log";

    @NotNull
    final public static String TASK_LOG_FILE_NAME = "./logger/task.log";

    @NotNull
    final public static String SESSION_LOG_FILE_NAME = "./logger/session.log";

    @NotNull
    final public static String USER_LOG_FILE_NAME = "./logger/user.log";

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final LogDto message) {
        @Nullable final String className = message.getClassName();
        @Nullable final String fileName = getFileName(className);
        if (fileName == null) return;
        @NotNull final File file = new File(fileName);
        file.getParentFile().mkdirs();
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file, true);
        @NotNull final String header = "Id: " + message.getId() + "; Type: " + message.getType() + "; Date: " + message.getDate() + "\n";
        fileOutputStream.write(header.getBytes());
        fileOutputStream.write(message.getEntity().getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    private String getFileName(@NotNull final String className) {
        switch (className) {
            case "Project":
            case "ProjectDto":
                return PROJECT_LOG_FILE_NAME;
            case "Task":
            case "TaskDto":
                return TASK_LOG_FILE_NAME;
            case "User":
            case "UserDto":
                return USER_LOG_FILE_NAME;
            case "Session":
            case "SessionDto":
                return SESSION_LOG_FILE_NAME;
            default:
                return null;
        }
    }

}

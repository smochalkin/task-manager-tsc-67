package ru.smochalkin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public class UserLockByLoginListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    @NotNull
    public String description() {
        return "User locking by login.";
    }

    @Override
    @EventListener(condition = "@userLockByLoginListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final Result result = adminEndpoint.lockUserByLogin(sessionService.getSession(), login);
        printResult(result);
    }

}

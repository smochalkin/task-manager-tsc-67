package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    void addAll(Collection<Project> collection);

    Project save(Project entity);

    void create();

    Project findById(String id);

    void clear();

    void removeById(String id);

    void remove(Project entity);

}

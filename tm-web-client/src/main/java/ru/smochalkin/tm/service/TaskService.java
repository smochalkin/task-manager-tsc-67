package ru.smochalkin.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.repository.ITaskRepository;

import java.util.Collection;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository repository;

    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Override
    public void addAll(final Collection<Task> collection) {
        if (collection == null) return;
        for (Task item : collection) {
            save(item);
        }
    }

    @Override
    public Task save(final Task entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Override
    public void create() {
        repository.save(new Task("new task"));
    }

    @Override
    public Task findById(final String id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public void removeById(final String id) {
        repository.deleteById(id);
    }

    @Override
    public void remove(final Task entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }

}

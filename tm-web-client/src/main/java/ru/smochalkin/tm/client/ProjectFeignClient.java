package ru.smochalkin.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.smochalkin.tm.api.endpoint.IProjectEndpoint;
import ru.smochalkin.tm.model.Project;

import java.util.List;

public interface ProjectFeignClient extends IProjectEndpoint {

    String URL = "http://localhost:8080/api/projects";

    static ProjectFeignClient client() {
        final FormHttpMessageConverter converter =
                new FormHttpMessageConverter();
        final HttpMessageConverters converters =
                new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory =
                () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectFeignClient.class, URL);
    }

    @GetMapping("/findAll")
    List<Project> findAll();

    @GetMapping("/find/{id}")
    Project find(@PathVariable("id") final String id);

    @PostMapping("/create")
    Project create(@RequestBody final Project project);

    @PostMapping("/createAll")
    List<Project> createAll(@RequestBody final List<Project> projects);

    @PutMapping("/save")
    Project save(@RequestBody final Project project);

    @PutMapping("/saveAll")
    List<Project> saveAll(@RequestBody final List<Project> projects);

    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable("id") final String id);

    @DeleteMapping("/deleteAll")
    void deleteAll();

}

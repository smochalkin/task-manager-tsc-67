package ru.smochalkin.tm.repository;

import org.springframework.stereotype.Repository;
import ru.smochalkin.tm.model.Project;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository
public class ProjectRepository {

    private final Map<String, Project> projects = new HashMap<>();

    {
        add("project 1");
        add("project 2");
        add("project 3");
    }

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void add(final String name) {
        final Project project = new Project(name);
        projects.put(project.getId(), project);
    }

    public void removeById(final String id) {
        projects.remove(id);
    }

    public List<Project> findAll() {
        List<Project> result = new ArrayList<>(projects.values());
        return result;
    }

    public Project findById(final String id) {
        return projects.get(id);
    }

    public void save(final Project project) {
        projects.put(project.getId(), project);
    }

    public void saveAll(final List<Project> list) {
        projects.putAll(list.stream().collect(Collectors.toMap(Project::getId, Function.identity())));
    }

    public void removeAll() {
        projects.clear();
    }

}